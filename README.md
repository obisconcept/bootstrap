# Neos CMS - Bootstrap Package
A package for Neos CMS to integrate some basic stuff like jQuery or Twitter Bootstrap.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/bootstrap": "~1.1"
}
```

## Generating Assets

- Installing NodeJs (https://nodejs.org/en/download/)
- Starting watcher in root path of the project: webpack --progress --colors --watch

## Version History

### Changes in 1.1.7
- Font Awesome 4.5.0
- Twitter Bootstrap 3.3.6

### Changes in 1.1.3
- Font path bugfix

### Changes in 1.1.2
- Bugfix for changeBootstrap function

### Changes in 1.1.1
- Bugfix TypoScript asset naming

### Changes in 1.1.0
- Added bundled assets. Complete rebuilding

### Changes in 1.0.9
- Bugfix for tabs

### Changes in 1.0.8
- Updated composer.json

### Changes in 1.0.7
- Added Tabs NodeType

### Changes in 1.0.6
- Fixed alignments for images

### Changes in 1.0.5
- Added some social media icons to the Button NodeType

### Changes in 1.0.4
- Optimized Button NodeType
- Added img-responsive CSS class to images 
- Added some basic CSS stuff for Neos CMS NodeTypes

### Changes in 1.0.3
- Bugfix

### Changes in 1.0.2
- Added Button NodeType

### Changes in 1.0.1
- Added Table NodeType
- Changed Template of Menu NodeType
- Changed Template of Asset List NodeType

### Changes in 1.0.0
- Added jQuery 2.1.4
- Added Twitter Bootstrap 3.3.5
- Added Font Awesome 4.4.0
- Twitter Bootstrap Frontend & Backend Integration
- Twitter Bootstrap MultiColumn NodeType