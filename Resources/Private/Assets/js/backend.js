var backendBsHalfCss = '';
var backendBsFullCss = '';
var backendBsCss = '';
var backendBsHalfJs = '';
var backendBsFullJs = '';
var backendBsJs = '';

$(document).ready(function () {

    backendBsHalfCss = $('#bs-bundle-css').attr('href').replace('bundle-backend', 'bundle-backend-half');
    backendBsFullCss = $('#bs-bundle-css').attr('href').replace('bundle-backend', 'bundle-backend-full');
    backendBsCss = $('#bs-bundle-css').attr('href');
    backendBsHalfJs = $('#bs-bundle-js').attr('src').replace('bundle-backend', 'bundle-backend-half');
    backendBsFullJs = $('#bs-bundle-js').attr('src').replace('bundle-backend', 'bundle-backend-full');
    backendBsJs = $('#bs-bundle-js').attr('src');

    $('body').on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function(event) {

        if (event.target.localName == 'body') {

            changeBootstrap();

        }

    });

});

window.changeBootstrap = function() {

    var left = $('body').css('marginLeft').replace('px', '');
    var right = $('body').css('marginRight').replace('px', '');

    if (left > 0 && right == 0) {

        $('#bs-bundle-css').attr('href', backendBsHalfCss);
        $('#bs-bundle-js').attr('src', backendBsHalfJs);

    } else if (right > 0 && left == 0) {

        $('#bs-bundle-css').attr('href', backendBsHalfCss);
        $('#bs-bundle-js').attr('src', backendBsHalfJs);

    } else if (left > 0 && right > 0) {

        $('#bs-bundle-css').attr('href', backendBsFullCss);
        $('#bs-bundle-js').attr('src', backendBsFullJs);

    } else if (right == 0 && left == 0) {

        $('#bs-bundle-css').attr('href', backendBsCss);
        $('#bs-bundle-js').attr('src', backendBsJs);

    }

}