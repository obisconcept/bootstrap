var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var UglifyJsPlugin = require('webpack').optimize.UglifyJsPlugin;

var ROOT_PATH = path.resolve(__dirname);
var SRC_PATH = path.resolve(ROOT_PATH, 'Resources/Private/Assets');
var DIST_PATH = path.resolve(ROOT_PATH, 'Resources/Public');

module.exports = {
    context: SRC_PATH,
    entry: {
        normal: './entry-normal.js',
        'backend': './entry-backend.js',
        'backend-half': './entry-backend-half.js',
        'backend-full': './entry-backend-full.js'
    },
    output: {
        path: DIST_PATH,
        filename: 'js/bundle-[name].js',
        publicPath: '../'
    },
    resolve: {
        alias: {
            jquery: path.resolve(SRC_PATH, 'js', 'jquery.js')
        }
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file?name=fonts/[name].[ext]',
                include: SRC_PATH
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css?sourceMap&minimize!sass?sourceMap'),
                include: SRC_PATH
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('css/bundle-[name].css'),
        new UglifyJsPlugin({
            sourceMap: true
        })
    ]
};